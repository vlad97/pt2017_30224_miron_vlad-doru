package UI;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame{
	
	private JFrame frame = new JFrame("Example");
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JPanel p4 = new JPanel();
	private JTextField tf = new JTextField(25);
	private JTextField tf1 = new JTextField(25);
	private JButton bs = new JButton("Suma");
	private JButton bd = new JButton("Diferenta");
	private JButton bp = new JButton("Produs");
	private JTextField tf3 = new JTextField(25);
	
	public View(){
		
		frame.setLayout(new GridLayout(4,1));
		p1.setLayout(new FlowLayout());
		p2.setLayout(new FlowLayout());
		JLabel label = new JLabel("Polinom 1: ");
		JLabel label1 = new JLabel("Polinom 2: ");
		JLabel l = new JLabel("Rezultat: ");
		p1.add(label);
		p1.add(tf);
		
		p2.add(label1);
		p2.add(tf1);
		
		p3.add(bs);
		p3.add(bd);
		p3.add(bp);
		
		p4.add(l);
		p4.add(tf3);
	
		frame.add(p1);
		frame.add(p2);
		frame.add(p3);
		frame.add(p4);
		
		frame.setSize(700,400);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
	}

	public void updateResult (String s)
	{
		tf3.setText(s);
	}
	
	public void setButonSumaListener(ActionListener x)
	{
		bs.addActionListener(x);
	}
	
	public void setButonDiferentaListener(ActionListener x)
	{
		bd.addActionListener(x);
	}
	
	public void setButonProdusListener(ActionListener x)
	{
		bp.addActionListener(x);
	}
	
	public String getTextP1()
	{
		return tf.getText();
	}
	
	public String getTextP2()
	{
		return tf1.getText();
	}
}
