package pt.assign1.logic;

import pt.assign1.model.Monom;
import pt.assign1.model.Polynom;

public class Operation {

	public static Polynom addOperation(Polynom t1, Polynom t2) {
		Polynom t3 = t2.copy();
		for (Monom m : t1.terms) {
			t3.addTerm(m);

		}
		return t3;
	}

	public static Polynom subOperation(Polynom t1, Polynom t2) {
		Polynom t3 = t2.copy();
		Polynom t4 = t1.copy();
		for (Monom m : t4.terms) {
			m.coef = m.coef * (-1);
			t3.addTerm(m);
		}

		return t3;
	}
	
	
	public static Polynom mulOperation(Polynom p1,Polynom p2)
	{
		Polynom rez=new Polynom();
		for(Monom m:p1.terms)
		{
			for(Monom n:p2.terms)
			{
				rez.addTerm(new Monom(m.coef*n.coef,m.power+n.power));
			}
		}
		return rez;
	}

}
