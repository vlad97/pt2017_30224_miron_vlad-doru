package pt.assign1.model;

public class Monom {
	public double coef;
	public int power;
	
	public Monom(double coef,int power){
		this.coef=coef;
		this.power=power;
	}

	@Override
	public String toString() {
		return this.coef + "x^" + this.power + " + ";
	}
	

	
	
}
