package test_main;


import pt.assign1.logic.Operation;
import pt.assign1.model.Monom;
import pt.assign1.model.Polynom;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Polynom p = new Polynom();
		Polynom p1 = new Polynom();
		
		p.addTerm(new Monom(2,4));
		p.addTerm(new Monom(1,3));
		p1.addTerm(new Monom(3,4));
		p1.addTerm(new Monom(4,3));
		
		Polynom p2= Operation.addOperation(p,p1);
		System.out.println(p2);
		
		Polynom p3 = Operation.subOperation(p, p1);
		System.out.println(p3);
		
		Polynom p4 = Operation.mulOperation(p,p1);
		System.out.println(p4);
		
	}

}
