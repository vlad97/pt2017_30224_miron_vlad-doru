package pt.assign1.model;

import java.util.*;

public class Polynom {
	public List<Monom> terms;

	public Polynom() {
		terms = new ArrayList<Monom>();
	}

	public void addTerm(Monom t) {
		boolean ok = false;
		for (Monom m : terms) {
			if (t.power == m.power) {
				m.coef = t.coef + m.coef;
				ok = true;
				break;
			}
		}

		if (ok != true) {
			terms.add(t);
		}
	}


	public String toString() {
		String s = "";

		for (Monom m : terms) {
			s = s + m.toString();
		}
		return s;

	}
	
	public Polynom copy(){
		Polynom p = new Polynom();
		
		for(Monom m : terms){
			p.addTerm(new Monom(m.coef,m.power));
		}
		return p;
	}

}
