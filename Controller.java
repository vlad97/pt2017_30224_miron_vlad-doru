package UI;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



import pt.assign1.logic.Operation;
import pt.assign1.model.Monom;
import pt.assign1.model.Polynom;

public class Controller  {

	private View view;

	public Controller(View view) {
		this.view = view;
		setupControl();
	}

	public void setupControl() {

		ActionListener calculSuma = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String input = view.getTextP1();
				String input1 = view.getTextP2();

				Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
				Matcher m = p.matcher( input );
				Matcher n = p.matcher( input1 );
				Polynom p1 = new Polynom();
				Polynom p2 = new Polynom();
				while (m.find()){
					p1.addTerm(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
				}
				while(n.find()){
					p2.addTerm(new Monom(Double.parseDouble(n.group(1)),Integer.parseInt(n.group(2))));
				}

				Polynom tf3 = Operation.addOperation(p1,p2);
				String result = tf3.toString();
				result =  result.substring(0, result.length()-2);
				view.updateResult(result);
			}

		};

		view.setButonSumaListener(calculSuma);
		
		
		ActionListener calculDiferenta = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String input = view.getTextP1();
				String input1 = view.getTextP2();

				Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
				Matcher m = p.matcher( input );
				Matcher n = p.matcher( input1 );
				Polynom p1 = new Polynom();
				Polynom p2 = new Polynom();
				while (m.find()){
					p1.addTerm(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
				}
				while(n.find()){
					p2.addTerm(new Monom(Double.parseDouble(n.group(1)),Integer.parseInt(n.group(2))));
				}

				Polynom tf3 = Operation.subOperation(p1,p2);
				String result = tf3.toString();
				result =  result.substring(0, result.length()-2);
				view.updateResult(result);
			}

		};

		view.setButonDiferentaListener(calculDiferenta);
		
		ActionListener calculProdus = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String input = view.getTextP1();
				String input1 = view.getTextP2();

				Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
				Matcher m = p.matcher( input );
				Matcher n = p.matcher( input1 );
				Polynom p1 = new Polynom();
				Polynom p2 = new Polynom();
				while (m.find()){
					p1.addTerm(new Monom(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2))));
				}
				while(n.find()){
					p2.addTerm(new Monom(Double.parseDouble(n.group(1)),Integer.parseInt(n.group(2))));
				}

				Polynom tf3 = Operation.mulOperation(p1,p2);
				String result = tf3.toString();
				result =  result.substring(0, result.length()-2);
				view.updateResult(result);
			}

		};

		view.setButonProdusListener(calculProdus);
		

	}



	public static void main(String[] args) {


		View view = new View();
		Controller c = new Controller(view);

	}
}

